﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RifaParticipantes.Models
{
    public class Participantes
    {
        public int Id { get; set; }

        public string Nombre { get; set; }

        public string Dpi { get; set; }

        public string Direccion { get; set; }

        public string Telefono { get; set; }

        public string correo { get; set; }
    }

}
