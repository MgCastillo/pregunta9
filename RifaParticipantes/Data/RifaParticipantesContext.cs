﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RifaParticipantes.Models;

namespace RifaParticipantes.Data
{
    public class RifaParticipantesContext : DbContext
    {
        public RifaParticipantesContext (DbContextOptions<RifaParticipantesContext> options)
            : base(options)
        {
        }

        public DbSet<RifaParticipantes.Models.Participantes> Participantes { get; set; }
    }
}
